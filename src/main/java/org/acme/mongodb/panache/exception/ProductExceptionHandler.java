package org.acme.mongodb.panache.exception;
 
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
 
@Provider
public class ProductExceptionHandler implements ExceptionMapper<ProductException>
{
    @Override
    public Response toResponse(ProductException exception)
    {
        return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build();  
    }
}