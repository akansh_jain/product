package org.acme.mongodb.panache;

import org.acme.mongodb.panache.exception.ProductException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/products")
@Consumes("application/json")
@Produces("application/json")
public class ProductResource {

    @Inject
    ProductService productService;

    @GET
    public List<Product> list() {
        return Product.listAll();
    }

    @GET
    @Path("/{id}")
    public Product get(@PathParam("id") Integer id) throws ProductException {
        return productService.get(id);
    }

    @POST
    public Response create(Product product) throws ProductException {
        productService.create(product);
        return Response.status(201).build();
    }

    @PUT
    @Path("/{id}")
    public void update(@PathParam("id") Integer id, Product productUpdated) throws ProductException {
        productService.update(id, productUpdated);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Integer id) throws ProductException {
        productService.delete(id);
    }

    @PUT
    @Path("/link/{id1}/{id2}")
    public void associateProducts(@PathParam("id1") Integer id1, @PathParam("id2") Integer id2) throws ProductException {
        productService.linkProducts(id1, id2);
    }

    @GET
    @Path("/search/{name}")
    public Product search(@PathParam("name") String name) {
        return Product.findByName(name);
    }

    @GET
    @Path("/count")
    public Long count() {
        return Product.count();
    }

}
