package org.acme.mongodb.panache;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Id;
import java.util.List;

@MongoEntity(collection = "Products")
public class Product extends PanacheMongoEntity {

    @Id
    public Integer id;
    public String name;
    public String description;
    public Double cost;
    public Integer quantity;
    public Integer height;
    public Integer width;
    public Integer length;
    @JsonbTransient
    public List<Integer> associatedItems;
    public List<Product> associatedProducts;

    // entity methods
    public static Product findByName(String name) {
        return find("name", name).firstResult();
    }

    public static List<Product> findByIds(Object ids) {
        return list("{'_id':{$in: [?1] }}",  ids);
    }


}
