##  Run below commands

docker run --network="product" --name=mongo -itv mongodata:/data/db3 -p 27017:27017 -d mongo:4.2.6

./mvnw package -Pnative -Dquarkus.native.container-build=true

docker build -f src/main/docker/Dockerfile.native -t product/quarkus-micro .

docker run --network="product" --rm -i -p 8080:8080 product/quarkus-micro

## API

postman api collection link : https://www.getpostman.com/collections/f0c526625e67e25b3a57