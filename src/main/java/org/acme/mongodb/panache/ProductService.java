package org.acme.mongodb.panache;

import com.mongodb.MongoWriteException;
import org.acme.mongodb.panache.exception.ProductException;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProductService {

    public Product get(Integer id) throws ProductException {
        Product product = Product.findById(id);
        if(Objects.isNull(product)){
            throw new ProductException("Product does not exist");
        }
        product.associatedProducts = Product.findByIds(product.associatedItems);
        removeStaleProductIds(product);
        return product;
    }

    public void create(Product product) throws ProductException {
        validateFields(product);
        try {
            product.persist();
        }catch (MongoWriteException e){
            throw new ProductException("The product already exists");
        }
    }

    private void validateFields(Product product) throws ProductException {
        List<String> messages = new ArrayList<>();
        if(Objects.isNull(product.quantity)){
            messages.add(" quantity must be given");
        }if(Objects.isNull(product.cost)){
            messages.add(" cost must be given");
        }if(Objects.isNull(product.description)){
            messages.add(" description must be given");
        }if(Objects.isNull(product.name)){
            messages.add(" name must be given");
        }if(Objects.isNull(product.height)){
            messages.add(" height must be given");
        }if(Objects.isNull(product.width)){
            messages.add(" width must be given");
        }if(Objects.isNull(product.length)){
            messages.add(" length must be given");
        }
        if(messages.size()>0){
            throw new ProductException(String.join(",",messages));
        }
    }

    public void delete(Integer id) throws ProductException {
        Product product = Product.findById(id);
        if(Objects.isNull(product)){
            throw new ProductException("Product does not exist");
        }
        product.delete();
    }

    public void update(Integer id, Product productUpdated) throws ProductException {
        Product product = Product.findById(id);
        if(Objects.isNull(product)){
            throw new ProductException("Product does not exist");
        }
        updateProductFields(product, productUpdated);
        product.update();
    }

    public void linkProducts(Integer id1, Integer id2) throws ProductException {
        Product product1 = Product.findById(id1);
        if(Objects.isNull(product1)){
            throw new ProductException("Product does not exist");
        }
        Product product2 = Product.findById(id2);
        if(Objects.isNull(product2)){
            throw new ProductException("Product does not exist");
        }
        addIfNotPresent(id2, product1);
        addIfNotPresent(id1, product2);
        product1.update();
        product2.update();
    }

    private void updateProductFields(Product product, Product productUpdated) {
        if(!Objects.isNull(productUpdated.cost)){
            product.cost = productUpdated.cost;
        }if(!Objects.isNull(productUpdated.description)){
            product.description = productUpdated.description;
        }if(!Objects.isNull(productUpdated.name)){
            product.name = productUpdated.name;
        }if(!Objects.isNull(productUpdated.quantity)) {
            product.quantity = productUpdated.quantity;
        }if(!Objects.isNull(productUpdated.height)){
            product.height = productUpdated.height;
        }if(!Objects.isNull(productUpdated.width)){
            product.width = productUpdated.width;
        }if(!Objects.isNull(productUpdated.length)){
            product.length = productUpdated.length;
        }
    }


    private void addIfNotPresent(Integer idToLink, Product product) {
        boolean exists = Boolean.FALSE;
        product.associatedItems = Objects.isNull(product.associatedItems)?
                new ArrayList<>():product.associatedItems;
        for (Integer id: product.associatedItems) {
            if (idToLink.equals(id)) {
                exists = Boolean.TRUE;
                break;
            }
        }
        if(!exists){
            product.associatedItems.add(idToLink);
        }

    }

    private void removeStaleProductIds(Product product) {
        List<Integer> existingIds = product.associatedProducts.stream()
                .map(item -> item.id).distinct().collect(Collectors.toList());

        if (validateIfNoStaleProductIds(product, existingIds)) return;

        product.associatedItems = existingIds;
        CompletableFuture.runAsync(product::update)
                .whenComplete((s,t)-> System.out.println("error while removing stale ids"));
    }

    private boolean validateIfNoStaleProductIds(Product product, List<Integer> existingIds) {
        Set<Integer> existingProductSet = new HashSet<>(existingIds);
        Set<Integer> currentProductSet = product.associatedItems==null?
                new HashSet<>(): new HashSet<>(product.associatedItems);
        return existingProductSet.equals(currentProductSet);
    }


}
