package org.acme.mongodb.panache;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static io.restassured.config.LogConfig.logConfig;
import static org.acme.mongodb.panache.MongoDbContainer.MONGODB_HOST;
import static org.acme.mongodb.panache.MongoDbContainer.MONGODB_PORT;
import static org.hamcrest.CoreMatchers.is;

@Testcontainers
@QuarkusTest
class ProductResourceTest {

    @Container
    static GenericContainer MONGO_DB_CONTAINER = new MongoDbContainer()
            .withCreateContainerCmdModifier(cmd -> cmd.withHostName(MONGODB_HOST)
                    .withPortBindings(new PortBinding(Ports.Binding.bindPort(MONGODB_PORT), new ExposedPort(MONGODB_PORT))));

    @BeforeAll
    static void initAll() {
        System.out.println(MONGO_DB_CONTAINER.getExposedPorts());
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config
                .logConfig((logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))
                .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((type, s) -> new ObjectMapper()
                        .registerModule(new Jdk8Module())
                        .registerModule(new JavaTimeModule())
                        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)));
    }

    @Test
    void createAndGetTest() {
        String product1 = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\" , " +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        String product2 = "{ \"id\" : \"4\", \"name\" : \"Iphone 11\", \"description\" : \"phone by Apple\" , " +
                " \"cost\" : \"1300.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        String product3 = "{ \"id\" : \"5\", \"name\" : \"Google Pixel 5\", \"description\" : \"phone by google\" , " +
                " \"cost\" : \"1000.50\" ,  \"quantity\" : \"10\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product1)
                .when()
                .post("/products")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product2)
                .when()
                .post("/products")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product3)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        Product[] products = RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products")
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product[].class);

        Assertions.assertThat(products.length).isEqualTo(3);

        Product product = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/{id}", 3)
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product.class);

        Assertions.assertThat(product.id).isEqualTo(3);
        Assertions.assertThat(product.name).isEqualTo("Samsung Galaxy Note");
        Assertions.assertThat(product.cost).isEqualTo(800.50d);
        Assertions.assertThat(product.quantity).isEqualTo(5);
        Assertions.assertThat(product.description).isEqualTo("phone by samsung");
        Assertions.assertThat(product.height).isEqualTo(10);
        Assertions.assertThat(product.width).isEqualTo(5);
        Assertions.assertThat(product.length).isEqualTo(5);

        Long count = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/count")
                .then()
                .statusCode(200)
                .extract()
                .body().as(Long.class);

        Assertions.assertThat(count).isGreaterThan(0);
    }


    @Test
    void resourceAlreadyCreatedTest() {
        String product = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post("/products")
                .then()
                .statusCode(400)
                .body(is("The product already exists"));

    }


    @Test
    void resourceNotFoundTest() {
        String product = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/{id}", 5)
                .then()
                .statusCode(400)
                .body(is("Product does not exist"));

    }

    @Test
    void updateTest() {
        String product = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        String productDto = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"1200.50\" ,  \"quantity\" : \"10\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(productDto)
                .when()
                .put("/products/{id}", 3)
                .then()
                .statusCode(204);

        Product productEntity = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/{id}", 3)
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product.class);

        Assertions.assertThat(productEntity.id).isEqualTo(3);
        Assertions.assertThat(productEntity.name).isEqualTo("Samsung Galaxy Note");
        Assertions.assertThat(productEntity.cost).isEqualTo(1200.50);
        Assertions.assertThat(productEntity.quantity).isEqualTo(10);
        Assertions.assertThat(productEntity.description).isEqualTo("phone by samsung");
        Assertions.assertThat(productEntity.height).isEqualTo(10);
        Assertions.assertThat(productEntity.width).isEqualTo(5);
        Assertions.assertThat(productEntity.length).isEqualTo(5);

    }


    @Test
    void deleteTest() {
        String product1 = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        String product2 = "{ \"id\" : \"4\", \"name\" : \"Iphone 11\", \"description\" : \"phone by Apple\"," +
                " \"cost\" : \"1300.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product1)
                .when()
                .post("/products")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product2)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        Product[] products = RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products")
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product[].class);

        Assertions.assertThat(products.length).isEqualTo(2);

        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .delete("/products/{id}", 3)
                .then()
                .statusCode(204);

        products = RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products")
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product[].class);

        Assertions.assertThat(products.length).isEqualTo(1);
    }



    @Test
    void associateTest() {
        String product1 = "{ \"id\" : \"3\", \"name\" : \"Samsung Galaxy Note\", \"description\" : \"phone by samsung\"," +
                " \"cost\" : \"800.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        String product2 = "{ \"id\" : \"4\", \"name\" : \"Iphone 11\", \"description\" : \"phone by Apple\"," +
                " \"cost\" : \"1300.50\" ,  \"quantity\" : \"5\" , \"height\" : \"10\" ," +
                "  \"width\" : \"5\",  \"length\" : \"5\"  }";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product1)
                .when()
                .post("/products")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product2)
                .when()
                .post("/products")
                .then()
                .statusCode(201);

        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .put("/products/link/{id1}/{id2}", 3,4)
                .then()
                .statusCode(204);

        Product productWithId3 = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/{id}", 3)
                .then()
                .statusCode(200)
                .extract()
                .body().as(Product.class);

        Assertions.assertThat(productWithId3.associatedProducts.size()).isEqualTo(1);
        Product associatedProduct = productWithId3.associatedProducts.stream().findFirst().get();

        Assertions.assertThat(associatedProduct.id).isEqualTo(4);
        Assertions.assertThat(associatedProduct.name).isEqualTo("Iphone 11");
        Assertions.assertThat(associatedProduct.cost).isEqualTo(1300.50d);
        Assertions.assertThat(associatedProduct.quantity).isEqualTo(5);
        Assertions.assertThat(associatedProduct.description).isEqualTo("phone by Apple");

    }

    @AfterEach
    public void tearDown(){
        Product.deleteAll();
    }

}
