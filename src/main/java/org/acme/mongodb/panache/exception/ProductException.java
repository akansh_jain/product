package org.acme.mongodb.panache.exception;

import java.io.Serializable;

public class ProductException extends Exception implements Serializable
{
    private static final long serialVersionUID = 1L;
 
    public ProductException() {
        super();
    }
    public ProductException(String msg)   {
        super(msg);
    }
    public ProductException(String msg, Exception e)  {
        super(msg, e);
    }
}